var express     = require('express');
var app         = express();
var bodyParser  = require('body-parser');



app.use( bodyParser.urlencoded({extended:true}));
app.use( bodyParser.json());


app.get('/', function(req,res){
	res.sendfile('./static/profile.html');
});


var port  = process.env.PORT || 8080;
var router = express.Router();

var mongoose    = require('mongoose');
//mongoose.connect('mongodb://tedcho:1234@ds033046.mlab.com:33046/lab3ece?authMechanism=SCRAM-SHA-1',function(err,db){
mongoose.connect('mongodb://localhost:27017/bears',function(err,db){
	console.log('Connected to DB');
	if (err) return console.log(err);
});


var Profile = require('./app/models/bear');

router.use( function(req, res, next){
    console.log('Something is happening');
    next();
});

router.get('/', function(req, res){
	res.sendFile('/static/index.html');
	res.json({message:'welcome to our api'});
	console.log('Router GET');
});

// creating bear
router.route('/bears')
    .post( function( req, res ){
    	console.log('BEARS POST');
    	var profile = new Profile();
    	profile.username	= req.body.username;
    	profile.age 		= req.body.age;
    	profile.gended		= req.body.gended;
    	profile.email		= req.body.email;
    	profile.picture		= req.body.picture;
    	
    	console.log(profile.username);
    	console.log(profile.age);
    	console.log(profile.gended);
    	console.log(profile.email);
    	console.log(profile.picture);
    	
    	
    	profile.save(function(err){
    		if(err){
    			res.send(err);	
    		} 
    		
    		res.json( {message:'Bear created', data:profile});
    	});
        
    })
    
    .get(function(req, res){
    	console.log('BEARS GET');
    	Profile.find(function(err,profiles){
    		if(err) res.send();
    		
    		res.json(profiles);
    	})
        
    })
    
    .put(function(req,res){
    	var profile = new Profile();
    	
    	profile.username	= req.body.username;
    	profile.age 		= req.body.age;
    	profile.gended		= req.body.gended;
    	profile.email		= req.body.email;
    	profile.picture		= req.body.picture;
    	
    	
    	profile.save(function(err){
    		if(err)
    			res.send(err);
    		res.json(profile);
    	})
    	
    });
    
// on routes that end in /bears/:bear_id
// ----------------------------------------------------
router.route('/bears/:bear_id')

	// get the profile with that id
	.get(function(req, res) {
		Profile.findById(req.params.profile_id, function(err, profile) {
			if (err){
				res.send(err);	
			} 
			
			res.json(profile);
		});
	})

	// update the bear with this id
	.put(function(req, res) {
		console.log(req.body);
		Profile.findById(req.params.bear_id, function(err, profile) {

			if (err)
				res.send(err);

			profile.username	= req.body.username;
			profile.age 		= req.body.age;
			profile.gended		= req.body.gended;
			profile.email		= req.body.email;
			profile.picture		= req.body.picture;
			profile.save(function(err) {
				if (err)
					res.send(err);

				res.json({ message: 'profile updated!' });
			});

		});
	})

	// delete the profile with this id
	.delete(function(req, res) {
		Profile.remove({
			_id: req.params.bear_id
		}, function(err, bear) {
			if (err){
				res.send(err);
			}

			res.json({ message: 'Successfully deleted' });
		});
	});
	

app.use('/api', router);

app.listen(port, function(){
	console.log('magic happends on port' + port);		
});


