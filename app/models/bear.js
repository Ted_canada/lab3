var mongoose    =   require('mongoose');
var Schema      =   mongoose.Schema;

var profileSchema  =   new Schema({
    username:{
        type: String
    },
    age:{
        type: String
    },
    gended:{
        type: String
    },
    email:{
        type: String
    },
    picture:{
        type: String
    }
 
});

module.exports =  mongoose.model('Profile', profileSchema);


